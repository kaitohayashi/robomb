﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeController : MonoBehaviour {

    [SerializeField] Fade fade;
    [SerializeField] int fadeTime = 1;

	// Use this for initialization
	void Start () {
        fade = this.GetComponent<Fade>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void changeSceneWithFade(string sceneName){
        fade.FadeIn(fadeTime, () =>
        {
            SceneManager.LoadScene(sceneName);
            fade.FadeOut(fadeTime);
        });
    }
}
