﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sidemove : MonoBehaviour {
    int g;
    float speed = 0.05f;
	// Use this for initialization
	void Start () {
        g = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(g == 1)
        {
            transform.position = transform.position + new Vector3(1, 0, 0) * speed;
            if (transform.position.x > 13)
                transform.position = new Vector3(-15, -3, 0);
        }
	}

    public void go()
    {
        g = 1;
    }

    public void Stop()
    {
        g = 0;
    }
}
