﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
	public GameObject _ctrlObj;
	public RobotOperation _operation;
    public float inputDelay = 1.0f;

	// Use this for initialization
	void Start ()
	{
		_ctrlObj = GameObject.FindGameObjectWithTag ("Robot");
		_operation = _ctrlObj.GetComponent <RobotOperation> ();
	}
	
	// Update is called once per frame
	//ここはバグシステムの組み込みがあるため，コルーチンに処理を渡した
	public virtual void Update ()
	{
        if (Input.GetKey(KeyCode.RightArrow))
            StartCoroutine("PushRight");
		if (Input.GetKey (KeyCode.LeftArrow))
            StartCoroutine("PushLeft");
		if (Input.GetKey (KeyCode.UpArrow))
            StartCoroutine("PushUp");
		if (Input.GetKey (KeyCode.DownArrow))
            StartCoroutine("PushDown");
		if (Input.GetKey (KeyCode.Z))
            StartCoroutine("PushZ");
		if (Input.GetKey (KeyCode.W))
            StartCoroutine("PushW");
		if (Input.GetKey (KeyCode.A))
            StartCoroutine("PushA");
		if (Input.GetKey (KeyCode.S))
            StartCoroutine("PushS");
		if (Input.GetKey (KeyCode.D))
            StartCoroutine("PushD");
	}

    /*この関数あんまり動いてるようにが見えないから意味ないと思う*/
    void Tremble(){
        int rand = Random.Range(0, 10); //rand = 0 ~ 9

        switch(rand){ //if(rand > 4) non operation
            case 0: _operation.PushRight(); break;
            case 1: _operation.PushLeft();  break;
            case 2: _operation.PushUp();    break;
            case 3: _operation.PushDown();  break;
        }
    }

    IEnumerator PushRight(){
        yield return new WaitForSeconds(inputDelay); //遅延処理　
        _operation.PushRight();
    }

    IEnumerator PushLeft()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushLeft();
    }

    IEnumerator PushUp()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushUp();
    }

    IEnumerator PushDown()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushDown();
    }

    IEnumerator PushZ()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushZ();
    }

    IEnumerator PushW()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushW();
    }

    IEnumerator PushA()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushA();
    }

    IEnumerator PushS()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushS();
    }

    IEnumerator PushD()
    {
        yield return new WaitForSeconds(inputDelay);
        _operation.PushD();
    }


}
