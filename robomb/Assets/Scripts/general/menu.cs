﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class menu : MonoBehaviour {
    [SerializeField] string MainScene = "ApproachBox"; 
    public GameObject[] sircle = new GameObject[3];
    public float speed;
    public int flag = 0;
    bool isKeyDown = false;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (flag == 0)
        {
            if (sircle[0].transform.localEulerAngles.z < 180)
                sircle[0].transform.Rotate(0, 0, speed);
            else if (sircle[1].transform.localEulerAngles.z > 0.01)
                sircle[1].transform.Rotate(0, 0, -1 * speed);
            else if (sircle[2].transform.localEulerAngles.z < 180)
                sircle[2].transform.Rotate(0, 0, speed);
            else
            {
                GameObject[] things = GameObject.FindGameObjectsWithTag("UP");
                foreach (GameObject thi in things)
                {
                    Vector3 pos = thi.transform.position;
                    if (pos.y > 20)
                        Destroy(thi);
                    else
                    {
                        pos.y += speed * 0.3f;
                        thi.transform.position = pos;
                    }
                }
                GameObject[] things2 = GameObject.FindGameObjectsWithTag("DOWN");
                foreach (GameObject thi2 in things2)
                {
                    Vector3 pos = thi2.transform.position;
                    if (pos.y < -20)
                    {
                        Destroy(thi2);
                        flag = 1;
                    }
                    else
                    {
                        pos.y -= speed * 0.3f;
                        thi2.transform.position = pos;
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                GameObject[] things = GameObject.FindGameObjectsWithTag("UP");
                foreach (GameObject thi in things)
                    Destroy(thi);
                GameObject[] things2 = GameObject.FindGameObjectsWithTag("DOWN");
                foreach (GameObject thi2 in things2)
                    Destroy(thi2);

                flag = 1;
            }

        }
        else
        {
            if (flag == 1)
            {
                GameObject T1 = GameObject.FindGameObjectWithTag("movetext");
                T1.transform.localPosition = new Vector3(0, 100, 0);
                flag = 2;
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                SceneManager.LoadScene(MainScene);
            }
        }

        if(Input.GetKeyDown(KeyCode.Z) && !isKeyDown){
            isKeyDown = true;
           GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade(MainScene);
        }
	}
}
