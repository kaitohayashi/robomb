﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bcolor : MonoBehaviour {
    float t = 0;
    float color;
    public GameObject N;
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 0.5f + Random.value * 0.5f, 0, 1);
    }
	
	// Update is called once per frame
	void Update () {
        transform.localScale += new Vector3(0.16f, 0.09f, 0) * 5;

        t += Time.deltaTime;
        if (t > 1)
        {
            color = Random.value; 
            Instantiate(N, transform.position, Quaternion.identity);
            GameObject[] things2 = GameObject.FindGameObjectsWithTag("stop");
            foreach (GameObject thi2 in things2)
            {
                if (color < 0.33f)
                    thi2.GetComponent<SpriteRenderer>().color = new Color(0.07676221f, 0.7075472f, 0.07676221f, 1);
                else if(color > 0.67f)
                    thi2.GetComponent<SpriteRenderer>().color = new Color(0.2684579f, 0.5849056f, 0.008276951f, 1);
                else
                    thi2.GetComponent<SpriteRenderer>().color = new Color(0.1233535f, 0.3396226f, 0.2124608f, 1);
            }

            GameObject thing = GameObject.FindGameObjectWithTag("background");
            if (color < 0.33f)
                thing.GetComponent<SpriteRenderer>().color = new Color(0.4816661f, 0.4923268f, 0.8301887f, 1);
            else if (color > 0.67f)
                thing.GetComponent<SpriteRenderer>().color = new Color(0.9528302f, 0.6912969f, 0.112362f, 1);
            else
                thing.GetComponent<SpriteRenderer>().color = new Color(0.2101308f, 0.1585974f, 0.4150943f, 1);
            Destroy(gameObject);
        }
    }
}
