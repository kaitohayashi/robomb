﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ncolor : MonoBehaviour {
    float t = 0;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, 1);
	}
	
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 1-t);
    }
}
