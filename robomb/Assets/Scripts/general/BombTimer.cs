﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BombTimer : MonoBehaviour {

    [SerializeField] Text text;
    [SerializeField] float count = 499f;

	// Use this for initialization
	void Start () {
        text = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        count -= Time.deltaTime;
        text.text = string.Format("{0:00}:{1:00}", (int)count/60, (int)count%60);

        if((int)count % 60 == 0){
            GameObject.Find("EventListener").GetComponent<EventListener>().timeFlagUp((int)count/60);
        }

        if (count > 60)
            text.color = Color.green;
        else if (count > 0)
            text.color = Color.red;
        else
        {
            SceneManager.LoadScene("GameOver");
            Destroy(gameObject);
        }
	}
}
