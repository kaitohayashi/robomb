﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class UIDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (SceneManager.GetSceneByName("GameOver").IsValid() || SceneManager.GetSceneByName("GameClear").IsValid())
            Destroy(gameObject);
	}
}
