﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create_object : MonoBehaviour {
    public GameObject robo,leaves;
    public GameObject[] box = new GameObject[3];
    float T = 0;
    float which;
    int flag = 1;
    int count = 0;
	// Use this for initialization
	void Start () {
        Instantiate(robo, new Vector3(2, -3, 0), Quaternion.identity);
        Instantiate(leaves, new Vector3(7, -3, 0), Quaternion.identity);
        Instantiate(leaves, new Vector3(0, -3, 0), Quaternion.identity);
        Instantiate(leaves, new Vector3(-7, -3, 0), Quaternion.identity);
        Instantiate(leaves, new Vector3(-14, -3, 0), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
        T += Time.deltaTime;

        if (T > 9)
        {
            which = Random.value;
            if (which < 0.1f)
                Instantiate(box[2], new Vector3(-10, -3.5f, 0), Quaternion.identity);
            else if(which < 0.4f)
                Instantiate(box[1], new Vector3(-10, -3.5f, 0), Quaternion.identity);
            else
                Instantiate(box[0], new Vector3(-10, -3.5f, 0), Quaternion.identity);
            T = 0;
            flag = 1;
        }
        else if(T > 6 && flag == 0)
        {
            flag = 2;
            count++;

            GameObject Box = GameObject.FindGameObjectWithTag("box");
            BoxAction B1 = Box.GetComponent<BoxAction>();
            if (count >= 3)
            {
                which = Random.value - 0.03f + count * 0.01f;
                if (which > 0.25f)
                {
                    count = 0;
                    B1.bomb();
                }
                else
                    B1.nbomb();
            }
            else
                B1.nbomb();
        }
        else if (T > 3.5f)
        {
            GameObject[] things2 = GameObject.FindGameObjectsWithTag("stop");
            foreach (GameObject thi2 in things2)
            {
                sidemove S1 = thi2.GetComponent<sidemove>();
                S1.Stop();               
            }
            if (flag == 1)
            {
                GameObject Box = GameObject.FindGameObjectWithTag("box");
                BoxAction B1 = Box.GetComponent<BoxAction>();
                B1.stop();
                flag = 0;
            }
        }
        else
        {
            GameObject[] things2 = GameObject.FindGameObjectsWithTag("stop");
            foreach (GameObject thi2 in things2)
            {
                sidemove S1 = thi2.GetComponent<sidemove>();
                S1.go();
            }
        }
	}
}
