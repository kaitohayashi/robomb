﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventListener : MonoBehaviour {

    [SerializeField] MessageBox messageBox;
    [SerializeField] MessageBox utlBox;
    bool[] stageClearFlag = new bool[6];
    int time;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void flagUp(int stageNumber){
        if(stageClearFlag[stageNumber]){  //フラグ揚げの重複
            printErrer();
            return;
        }

        stageClearFlag[stageNumber] = true;

        switch( (10 - time) / (stageNumber + 1)){
            case 0: messageBox.SetMessage("いいペース", "default"); break;
            case 1: messageBox.SetMessage("速い", "default"); break;
            case 2: messageBox.SetMessage("普通", "default"); break;
            case 3: messageBox.SetMessage("もうすこしペースを上げよう", "default"); break;
            default : messageBox.SetMessage("いそげいそげ", "default"); break;
        }

        switch(stageNumber){
            case 0: utlBox.SetMessage("z：パンチ発動", "default"); break;
            case 1: utlBox.SetMessage("↑↓←→：移動 \n z：ドライバーの上げ下げ \n w→d→s→a：時計回し \n w→a→s→d：反時計回し", "default"); break;
            case 2: utlBox.SetMessage("↑↓←→：移動", "default"); break;
            case 3: utlBox.SetMessage("↑↓←→：移動 \n z：アームを降ろす", "default"); break;
            case 4: utlBox.SetMessage("↑↓←→：移動 \n a：反時計回し \n d:時計回し \n z：切る", "default"); break;
        }

    }

    public void timeFlagUp(int t){
        this.time = t;

        if(time < 0){ //ガード文
            return;
        }

        messageBox.SetMessage("あと" + time + "分", "default");
    }

    void printErrer(){
        messageBox.SetMessage("不具合が生じたので開発者に連絡してね", "default");
    }
}
