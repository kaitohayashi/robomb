﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxAction : MonoBehaviour {
    int m = 1,f = 1,g = 1;
    float speed = 0.05f,speed2 = 1;
    float t = 0;
    public GameObject effect;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = transform.position + new Vector3(1, 0, 0) * speed * m;
        if (f == 0)
        {
            if (g == 1)
                t += Time.deltaTime;
            else
                t -= Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, t * 8);
            if (t > 0.125)
                g = 0;
            if (t < 0)
                g = 1;
        }
        else
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

        if(f == 2)
        {
            t += Time.deltaTime;
            transform.position = transform.position + new Vector3(2, (speed2 - t) * 4, 0) * speed;
            gameObject.transform.Rotate(0, 0, -4);
        }
        if (f == 3)
        {
            t += Time.deltaTime;
            if (t >= 0.25f)
            {
                Instantiate(effect, transform.position, Quaternion.identity);
                t = 0;
            }
        }
    }

    public void stop()
    {
        m = 0;
        f = 0;
    }

    public void bomb()
    {
        f = 3;
        Destroy(gameObject, 3);       
    }

    public void nbomb()
    {
        f = 2;
        t = 0;
        Destroy(gameObject, 3);              
    }
}
