﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class successJudge : MonoBehaviour {

    [SerializeField] static List<bool> success = new List<bool>();
    [SerializeField] static int size = 8;
    [SerializeField] static string nextscene = "hoge";

	public static void Success(int idx){
        Debug.Log(idx);
        success[idx] = true;

        bool allTrue = true;
        foreach(bool b in success){
            if (!b) allTrue = false;
        }
        if(allTrue){
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade(nextscene);
        }

    }

    public  static void CreateSuccessJudge(int size, string sceneName){
        CreateJudgeArray(size);
        SetNextScene(sceneName);
    } 

    static void SetNextScene(string SceneName){
        successJudge.nextscene = SceneName;
    }

    static void CreateJudgeArray(int setsize){
        success.Clear();
        successJudge.size = setsize;
        for (int i = 0; i < size; i++)
            success.Add(false);
    }
}
