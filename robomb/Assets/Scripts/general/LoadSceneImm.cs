﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LoadSceneImm : MonoBehaviour {

    [SerializeField] string nextScene;

	// Use this for initialization
	void Start () {
        SceneManager.LoadScene(nextScene);
	}
}
