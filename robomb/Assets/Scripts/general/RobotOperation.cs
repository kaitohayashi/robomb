﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface RobotOperation
{
	void PushRight ();

	void PushLeft ();

	void PushUp ();

	void PushDown ();

	void PushZ ();

	void PushW ();

	void PushA ();

	void PushS ();

	void PushD ();
}
