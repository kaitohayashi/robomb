﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour, RobotOperation
{
	public Transform _transform = null;
	public float _speed = 1.0f;
    [SerializeField] float LIMIT_X = 8.5f;
    [SerializeField] float LIMIT_Y = 4.5f;

	// Use this for initialization
	void Start ()
	{
		_transform = GetComponent <Transform> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

    protected Vector3 ClampPosition(Vector3 position){
        float x = Mathf.Clamp(position.x, -LIMIT_X, LIMIT_X);
        float y = Mathf.Clamp(position.y, -LIMIT_Y, LIMIT_Y);
        float z = position.z;
        return new Vector3(x, y, z);
    }

	//move right
    public virtual void PushRight ()
	{
		Vector3 temp = _transform.position;
		temp.x += _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
	}

	//move left
    public virtual void PushLeft ()
	{
		Vector3 temp = _transform.position;
		temp.x -= _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
	}

	//move up
    public virtual void PushUp ()
	{
		Vector3 temp = _transform.position;
		temp.y += _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
	}

	//move down
    public virtual void PushDown ()
	{
		Vector3 temp = _transform.position;
		temp.y -= _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
	}

	public virtual void PushZ ()
	{
        //non operation
	}

	public virtual void PushW ()
	{
		//non operation
	}

	public virtual void PushA ()
	{
		//non operation
	}

	public virtual void PushS ()
	{
		//non operation
	}

	public virtual void PushD ()
	{
		//non operation
	}
}
