﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shake : MonoBehaviour {
    float a = 0;
    float speed = 4.0f;
    float bairitsu = 0.2f;
    Vector3 V_pos = new Vector3(0, 1, 0);
    Vector3 Fpos;
	// Use this for initialization
	void Start () {
        Fpos = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        a += Time.deltaTime;

        transform.position = Fpos + V_pos * Mathf.Sin(a * speed) * bairitsu;

        if (a > 9)
            a = 0;
        else if (a > 6)
            speed = 0;
        else if (a > 3.5f)
        {
            speed = 16.0f;
            bairitsu = 0.1f;
        }
        else {
            speed = 4.0f;
            bairitsu = 0.2f;
        }
	}
}
