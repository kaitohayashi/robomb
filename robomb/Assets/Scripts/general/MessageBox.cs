﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{

    [SerializeField] Text box;

    [SerializeField] string message = "sample text";

    // Use this for initialization
    void Start()
    {
        box.text = message;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetMessage(string msg, string face){
        message = msg;

        box.text = message;
    }
}
