﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class SkipScene : MonoBehaviour {

    [SerializeField] string nextScene;
    [SerializeField] int num;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            GameObject.Find("EventListener").GetComponent<EventListener>().flagUp(num);
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade(nextScene);
        }
	}
}
