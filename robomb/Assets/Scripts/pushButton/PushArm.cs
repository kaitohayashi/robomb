﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushArm : Robot {
    public GameObject pushHand;

    public override void PushZ(){
        Instantiate(pushHand, _transform.position, Quaternion.identity);   
    }

}
