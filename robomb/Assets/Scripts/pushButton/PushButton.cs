﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : MonoBehaviour {

    [SerializeField] int id = 0;
    [SerializeField] Sprite pushedButton;

	// Use this for initialization
	void Start () {
		
	}
	
    void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "PushHand")
        {
            Debug.Log("pushed");
            successJudge.Success(id);
            GetComponent<SpriteRenderer>().sprite = pushedButton;
            //成功判定を通知
            //押されたあとの処理
        }
    }
}
