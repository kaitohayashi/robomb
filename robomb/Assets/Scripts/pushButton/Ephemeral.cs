﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ephemeral : MonoBehaviour {
    [SerializeField] float limit = 1.0f; 
	// Use this for initialization
	void Start () {
        Destroy(gameObject, limit);
	}
}
