﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BreakingRobot : MonoBehaviour, RobotOperation {
    public float power = 0; //0~100
    public float maxPower = 100f;
    public float minPower = 0;
    public bool increase = true;
    public float gaugeSpeed = 1.0f;
    public float[] successRange = new float[2] { 30f, 70f };
    public Slider slider;
    public Animator animator;
    bool isPressed = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        VaryPowerGauge();
        slider.value = power;
	}

    void VaryPowerGauge()
    {
        /*ゲージを動かす*/
        if (increase)
        {
            power += Time.deltaTime * gaugeSpeed;
        }
        else
        {
            power -= Time.deltaTime * gaugeSpeed;
        }

        /*ゲージの末端まで到達した時の処理*/
        if (power > maxPower)
        { //上端
            power = maxPower - (power - maxPower); //ゲージを折り返す
            increase = false; //次から減少
        }
        if (power < minPower)
        { //下端
            power = -power; //ゲージを折り返す
            increase = true; //次から増加
        }

        if (power >= successRange[0] && power <= successRange[1])
        {
            animator.SetBool("hi", true);
        }
        else{
            animator.SetBool("hi", false);
        }
    }

    //Stop power gauge
    public void PushZ()
    {
        if (isPressed)
            return;

        isPressed = true;
        
        if (power >= successRange[0] && power <= successRange[1])
        {
            //成功アニメーション
            animator.SetTrigger("punch");
            sleep(10f);
            GameObject.Find("EventListener").GetComponent<EventListener>().flagUp(1);
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade("arms");
        }
        else
        {
            //失敗アニメーション
            animator.SetTrigger("punch");
            sleep(10f);
            SceneManager.LoadScene("GameOver");
        }
    }

    IEnumerator sleep(float time){
        yield return new WaitForSeconds(time);
    }

    public void PushRight()
    {
        //non operation
    }

    public void PushLeft()
    {
        //non operation
    }

    public void PushUp()
    {
        //non operation
    }

    public void PushDown()
    {
        //non operation
    }

    public void PushW()
    {
        //non operation
    }

    public void PushA()
    {
        //non operation
    }

    public void PushS()
    {
        //non operation
    }

    public void PushD()
    {
        //non operation
    }
}
