﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScissorsArm : Robot{

    [SerializeField] float rotateSpeed = 1.0f;
    [SerializeField] GameObject ScissorsHand;

	public override void PushA()
	{
        _transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
	}

    public override void PushD()
    {
        _transform.Rotate(Vector3.back * rotateSpeed * Time.deltaTime);
    }

	public override void PushZ()
	{
        Instantiate(ScissorsHand, _transform.position, _transform.rotation);
	}
}
