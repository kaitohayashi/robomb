﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutMark : MonoBehaviour {

    [SerializeField] int id = 0;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "CenterScissors")
        {
            Debug.Log("cut");
            successJudge.Success(id);
            GetComponent<SpriteRenderer>().sprite = null;
            //成功判定を通知
            //押されたあとの処理
        }
    }
}
