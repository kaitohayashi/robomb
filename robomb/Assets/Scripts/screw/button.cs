﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class button : MonoBehaviour {
    int a = 0;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        a = 1;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        a = 0;
    }

    public void Push()
    {
        if (a == 1)
            SceneManager.LoadScene("GameOver");
    }
}
