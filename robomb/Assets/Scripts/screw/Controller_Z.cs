﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Z : Controller {
   
    // Use this for initialization
	
	// Update is called once per frame
	public override void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
            StartCoroutine("PushRight");
        if (Input.GetKey(KeyCode.LeftArrow))
            StartCoroutine("PushLeft");
        if (Input.GetKey(KeyCode.UpArrow))
            StartCoroutine("PushUp");
        if (Input.GetKey(KeyCode.DownArrow))
            StartCoroutine("PushDown");
        if (Input.GetKeyDown(KeyCode.Z))
            StartCoroutine("PushZ");
        if (Input.GetKeyDown(KeyCode.W))
            StartCoroutine("PushW");
        if (Input.GetKeyDown(KeyCode.A))
            StartCoroutine("PushA");
        if (Input.GetKeyDown(KeyCode.S))
            StartCoroutine("PushS");
        if (Input.GetKeyDown(KeyCode.D))
            StartCoroutine("PushD");
    }

}
