﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class arms : Robot {

    [SerializeField] SpriteRenderer spriteRenderer;
    public Transform T1;
    public float speed;
    public GameObject Butten;
    public GameObject[] Screw = new GameObject[4];
    button B1;
    screw S1;
    int Z = 0; //0だと上下左右に動き、１だと固有操作
    int Eq = 1; //0:ドライバー,1:指,2:鍵,and more...

    int D_before = 0;
    int next_scene = 0;

    int i;

	// Use this for initialization
	void Start () {
        B1 = Butten.GetComponent<button>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void PushRight()
    {
        if(Z == 0)
        {
            Vector3 temp = T1.position;
            temp.x += speed * Time.deltaTime;
            T1.position = ClampPosition(temp);
        }
    }
    public override void PushLeft()
    {
        if (Z == 0)
        {
            Vector3 temp = T1.position;
            temp.x -= speed * Time.deltaTime;
            T1.position = ClampPosition(temp);
        }
    }
    public override void PushUp()
    {
        if (Z == 0)
        {
            Vector3 temp = T1.position;
            temp.y += speed * Time.deltaTime;
            T1.position = ClampPosition(temp);
        }
    }
    public override void PushDown()
    {
        if (Z == 0)
        {
            Vector3 temp = T1.position;
            temp.y -= speed * Time.deltaTime;
            T1.position = ClampPosition(temp);
        }
    }
    public override void PushZ()
    {
        if (Z == 0)
        {
            spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
            D_before = 0;
            Z = 1;
            B1.Push();
        }
        else
        {
            spriteRenderer.color = new Color(1f, 1f, 1f, 0.5f);
            Z = 0;
        }
    }
    public override void PushW()
    {
        int num = 1;
        if (Z == 0)
            return;
        else
        {
            if (Eq == 1)
                Driver(num);
        }
    }
    public override void PushA()
    {
        int num = 2;
        if (Z == 0)
            return;
        else
        {
            if (Eq == 1)
                Driver(num);
        }
    }
    public override void PushS()
    {
        int num = 3;
        if (Z == 0)
            return;
        else
        {
            if (Eq == 1)
                Driver(num);
        }
    }
    public override void PushD()
    {
        int num = 4;
        if (Z == 0)
            return;
        else
        {
            if (Eq == 1)
                Driver(num);
        }
    }

    public void Driver(int a)//1=W,2=A,3=S,4=D
    {
        if (D_before == 0)
            D_before = a;
        else
        {
            int mod = (a - D_before + 4) % 4;
            if (mod == 1)
            {
                for(i=0;i<=3;i++)
                {
                    S1 = Screw[i].GetComponent<screw>();
                    next_scene += S1.spin();
                }
            }
            else if (mod == 3) //(a - D_before + 4) % 4 == 3
            {
                for (i = 0; i <= 3; i++)
                {
                    S1 = Screw[i].GetComponent<screw>();
                    next_scene += S1.U_spin();
                }
            }
            D_before = a;
        }

        Debug.Log(next_scene);

        if (next_scene >= 4)
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade("Unlock"); //次のシーンへ
    }
}
