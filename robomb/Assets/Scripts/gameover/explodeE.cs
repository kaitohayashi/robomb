﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explodeE : MonoBehaviour {
    [SerializeField] AudioClip explosion;
    [SerializeField] AudioClip jingle;
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject text;
    [SerializeField] GameObject text2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void explode(){
        audioSource.PlayOneShot(explosion);
        audioSource.PlayOneShot(jingle);
    }

    public void textactive(){
        text.SetActive(true);
        text2.SetActive(true);
    }

}
