﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;


public class Keyhole : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Robot")
        {
            //成功アニメーション
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade("PushButton");
        }
    }
}
