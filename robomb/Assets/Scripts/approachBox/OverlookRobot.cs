﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlookRobot : Robot
{

    //move right
    public override void PushRight()
    {
        Vector3 temp = _transform.position;
        temp.x += _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
        transform.rotation = Quaternion.Euler (0f, 0f, -90f);
    }

    //move left
    public override void PushLeft()
    {
        Vector3 temp = _transform.position;
        temp.x -= _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
        transform.rotation = Quaternion.Euler(0f, 0f, 90f);
    }

    //move up
    public override void PushUp()
    {
        Vector3 temp = _transform.position;
        temp.y += _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    //move down
    public override void PushDown()
    {
        Vector3 temp = _transform.position;
        temp.y -= _speed * Time.deltaTime;
        _transform.position = ClampPosition(temp);
        transform.rotation = Quaternion.Euler(0f, 0f, 180f);
    }
}    
