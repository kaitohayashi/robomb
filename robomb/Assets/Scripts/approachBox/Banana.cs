﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Banana : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
        Debug.Log("trigger");
        if(other.tag == "Robot"){
            //ロボットが滑るアニメーション
            //ゲームオーバーシーンをロード
            SceneManager.LoadScene("GameOver");
        }
	}
}
