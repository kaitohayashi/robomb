﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Box : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
        if (collision.tag == "Robot"){
            GameObject.Find("EventListener").GetComponent<EventListener>().flagUp(0);
            GameObject.Find("FadeCanvas").GetComponent<FadeController>().changeSceneWithFade("BreakBox");
        }
	}
}
